#ifndef LIST_HPP_
#define LIST_HPP_

#include <memory>

#include "errors.hpp"

template <typename T>
class List
{
  public:
    class Node
    {
      public:
        T data_;
        std::shared_ptr<List<T>::Node> next_;
        std::shared_ptr<List<T>::Node> previous_;

        Node();
    };

    class Iterator
    {
        std::shared_ptr<Node> currentNode_;

      public:
        using difference_type = long;
        using value_type = T;
        using pointer = const std::shared_ptr<T>;
        using reference = const T&;
        using iterator_category = std::random_access_iterator_tag;

        explicit Iterator(std::shared_ptr<Node> node = nullptr);

        Iterator operator++();
        Iterator operator--();
        bool operator==(const Iterator& other) const;
        bool operator!=(const Iterator& other) const;
        bool operator>(const Iterator& other) const;
        bool operator<(const Iterator& other) const;
        difference_type operator-(const Iterator& other) const;
        Iterator operator+(difference_type diff) const;
        Iterator operator-(difference_type diff) const;
        Iterator operator[](std::size_t i);
        T& operator*();
    };

    class ConstIterator
    {
        std::shared_ptr<Node> currentNode_;

      public:
        using difference_type = long;
        using value_type = T;
        using pointer = const std::shared_ptr<T>;
        using reference = const T&;
        using iterator_category = std::random_access_iterator_tag;

        explicit ConstIterator(std::shared_ptr<Node> node = nullptr);

        ConstIterator operator++();
        ConstIterator operator--();
        bool operator==(const ConstIterator& other) const;
        bool operator!=(const ConstIterator& other) const;
        bool operator>(const ConstIterator& other) const;
        bool operator<(const ConstIterator& other) const;
        difference_type operator-(const ConstIterator& other) const;
        ConstIterator operator+(difference_type diff) const;
        ConstIterator operator-(difference_type diff) const;
        ConstIterator operator[](std::size_t i);
        const T& operator*();
    };

    List();
    ~List();

    void pushBack(const T& newElement);
    void pushFront(const T& newElement);
    void insert(const T& newElement, int index);
    void remove(const T& element);
    void removeOnce(const T& element);
    Iterator begin();
    Iterator end();
    ConstIterator cbegin() const;
    ConstIterator cend() const;
    T& operator[](int index);
    int size();
    bool isEmpty();

  private:
    std::shared_ptr<List<T>::Node> head_;
    std::shared_ptr<List<T>::Node> tail_;

    void removeElement(const std::shared_ptr<List<T>::Node>& element);
};

template <typename T>
List<T>::Node::Node()
{
    next_ = nullptr;
    previous_ = nullptr;
}

template <typename T>
List<T>::Iterator::Iterator(typename std::shared_ptr<Node> node)
{
    currentNode_ = node;
}

template <typename T>
typename List<T>::Iterator List<T>::Iterator::operator++()
{
    currentNode_ = currentNode_->next_;
    return Iterator{currentNode_};
}

template <typename T>
typename List<T>::Iterator List<T>::Iterator::operator--()
{
    currentNode_ = currentNode_->previous_;
    return Iterator{currentNode_};
}

template <typename T>
bool List<T>::Iterator::operator==(const Iterator& other) const
{
    return currentNode_ == other.currentNode_;
}

template <typename T>
bool List<T>::Iterator::operator!=(const Iterator& other) const
{
    auto iterator = Iterator(currentNode_);
    return !(iterator == other);
}

template <typename T>
bool List<T>::Iterator::operator>(const Iterator& other) const
{
    for(auto iterator = currentNode_; iterator != nullptr; iterator = iterator->next_)
    {
        if(iterator == other.currentNode_)
            return false;
    }

    return true;
}

template <typename T>
bool List<T>::Iterator::operator<(const Iterator& other) const
{
    auto iterator = Iterator(currentNode_);
    return !(iterator > other);
}

template <typename T>
typename List<T>::Iterator::difference_type List<T>::Iterator::operator-(const Iterator& other) const
{
    int counter = 0;
    auto iterator1 = Iterator(currentNode_);
    auto iterator2 = Iterator(other.currentNode_);

    if(iterator1 == iterator2)
    {
        counter = 0;
    }
    else
    {
        while(iterator1 != iterator2)
        {
            if(iterator1 > iterator2)
            {
                ++iterator2;
                ++counter;
            }
            else
            {
                ++iterator1;
                ++counter;
            }
        }
    }

    return counter;
}

template <typename T>
typename List<T>::Iterator List<T>::Iterator::operator+(difference_type diff) const
{
    auto iterator = Iterator(currentNode_);
    while(diff-- > 0)
        ++iterator;

    return Iterator{iterator};
}

template <typename T>
typename List<T>::Iterator List<T>::Iterator::operator-(difference_type diff) const
{
    auto iterator = Iterator(currentNode_);
    while(diff-- > 0)
        --iterator;

    return Iterator{iterator};
}

template <typename T>
typename List<T>::Iterator List<T>::Iterator::operator[](std::size_t i)
{
    auto element = Iterator(currentNode_);
    element = element + i;

    return Iterator{element};
}

template <typename T>
T& List<T>::Iterator::operator*()
{
    return currentNode_->data_;
}

template <typename T>
List<T>::ConstIterator::ConstIterator(typename std::shared_ptr<Node> node)
{
    currentNode_ = node;
}

template <typename T>
typename List<T>::ConstIterator List<T>::ConstIterator::operator++()
{
    currentNode_ = currentNode_->next_;
    return ConstIterator{currentNode_};
}

template <typename T>
typename List<T>::ConstIterator List<T>::ConstIterator::operator--()
{
    currentNode_ = currentNode_->previous_;
    return ConstIterator{currentNode_};
}

template <typename T>
bool List<T>::ConstIterator::operator==(const ConstIterator& other) const
{
    return currentNode_ == other.currentNode_;
}

template <typename T>
bool List<T>::ConstIterator::operator!=(const ConstIterator& other) const
{
    const auto iterator = ConstIterator(currentNode_);
    return !(iterator == other);
}

template <typename T>
bool List<T>::ConstIterator::operator>(const ConstIterator& other) const
{
    for(auto iterator = currentNode_; iterator != nullptr; iterator = iterator->next_)
    {
        if(iterator == other.currentNode_)
            return false;
    }

    return true;
}

template <typename T>
bool List<T>::ConstIterator::operator<(const ConstIterator& other) const
{
    auto iterator = ConstIterator(currentNode_);
    return !(iterator > other);
}

template <typename T>
typename List<T>::ConstIterator::difference_type List<T>::ConstIterator::operator-(const ConstIterator& other) const
{
    int counter = 0;
    auto iterator1 = ConstIterator(currentNode_);
    auto iterator2 = ConstIterator(other.currentNode_);

    if(iterator1 == iterator2)
    {
        counter = 0;
    }
    else
    {
        while(iterator1 != iterator2)
        {
            if(iterator1 > iterator2)
            {
                ++iterator2;
                ++counter;
            }
            else
            {
                ++iterator1;
                ++counter;
            }
        }
    }

    return counter;
}

template <typename T>
typename List<T>::ConstIterator List<T>::ConstIterator::operator+(difference_type diff) const
{
    auto iterator = ConstIterator(currentNode_);
    while(diff-- > 0)
        ++iterator;

    return ConstIterator{iterator};
}

template <typename T>
typename List<T>::ConstIterator List<T>::ConstIterator::operator-(difference_type diff) const
{
    auto iterator = ConstIterator(currentNode_);
    while(diff-- > 0)
        --iterator;

    return ConstIterator{iterator};
}

template <typename T>
typename List<T>::ConstIterator List<T>::ConstIterator::operator[](std::size_t i)
{
    auto element = ConstIterator(currentNode_);
    element = element + i;

    return ConstIterator{element};
}

template <typename T>
const T& List<T>::ConstIterator::operator*()
{
    return currentNode_->data_;
}

template <typename T>
List<T>::List()
{
    head_ = nullptr;
    tail_ = nullptr;
}

template <typename T>
List<T>::~List()
{
    std::shared_ptr<List<T>::Node> tmp;
    while(head_ != nullptr)
    {
        tmp = head_;
        head_ = head_->next_;
        tmp->next_ = nullptr;
        tmp->previous_ = nullptr;
    }
}

template <typename T>
void List<T>::pushBack(const T& newElement)
{
    auto newNode = static_cast<std::shared_ptr<List<T>::Node>>(new List<T>::Node);

    if(newNode == nullptr)
        exit(E_ALLOCATION_MEMORY);

    newNode->data_ = newElement;

    if(head_ == nullptr)
    {
        head_ = newNode;
        tail_ = newNode;
    }
    else
    {
        tail_->next_ = newNode;
        newNode->previous_ = tail_;
        tail_ = newNode;
    }
}

template <typename T>
void List<T>::pushFront(const T& newElement)
{
    auto newNode = static_cast<std::shared_ptr<List<T>::Node>>(new List<T>::Node);

    if(newNode == nullptr)
        exit(E_ALLOCATION_MEMORY);

    newNode->data_ = newElement;

    if(head_ == nullptr)
    {
        head_ = newNode;
        tail_ = newNode;
    }

    newNode->next_ = head_;
    newNode->previous_ = nullptr;

    if(head_ != nullptr)
        head_->previous_ = newNode;

    head_ = newNode;
}

template <typename T>
void List<T>::insert(const T& newElement, int index)
{
    auto newNode = static_cast<std::shared_ptr<List<T>::Node>>(new List<T>::Node);

    if(newNode == nullptr)
        exit(E_ALLOCATION_MEMORY);

    newNode->data_ = newElement;

    if(head_ == nullptr)
    {
        head_ = newNode;
        tail_ = newNode;
    }
    else
    {
        if(index > size())
        {
            pushBack(newElement);
        }
        else
        {
            auto tmp = head_;
            for(int i = 0; i < index; ++i)
                tmp = tmp->next_;

            if(tmp->previous_ == nullptr)
            {
                pushFront(newElement);
            }
            else
            {
                newNode->next_ = tmp;
                newNode->previous_ = tmp->previous_;
                tmp->previous_->next_ = newNode;
                tmp->previous_ = newNode;
            }
        }
    }
}

template <typename T>
void List<T>::removeElement(const std::shared_ptr<List<T>::Node>& element)
{
    if(element->previous_ == nullptr && element->next_ == nullptr)
    {
        head_ = nullptr;
        tail_ = nullptr;
    }
    else if(element->next_ == nullptr)
    {
        element->previous_->next_ = element->next_;
        tail_ = element->previous_;
    }
    else if(element->previous_ == nullptr)
    {
        head_ = element->next_;
        element->next_->previous_ = nullptr;
    }
    else
    {
        element->next_->previous_ = element->previous_;
        element->previous_->next_ = element->next_;
    }
}

template <typename T>
void List<T>::remove(const T& element)
{
    if(head_ == nullptr)
        exit(E_MODIFYING_ITEM);

    for(auto tmp = head_; tmp->next_ != nullptr; tmp = tmp->next_)
    {
        if(tmp->data_ == element)
            removeElement(tmp);
    }
}

template <typename T>
void List<T>::removeOnce(const T& element)
{
    if(head_ == nullptr)
        exit(E_MODIFYING_ITEM);

    std::shared_ptr<List<T>::Node> tmp;
    for(tmp = head_; tmp->data_ != element; tmp = tmp->next_)
        ;

    removeElement(tmp);
}

template <typename T>
typename List<T>::Iterator List<T>::begin()
{
    return Iterator{head_};
}

template <typename T>
typename List<T>::Iterator List<T>::end()
{
    return Iterator{nullptr};
}

template <typename T>
typename List<T>::ConstIterator List<T>::cbegin() const
{
    return ConstIterator{head_};
}

template <typename T>
typename List<T>::ConstIterator List<T>::cend() const
{
    return ConstIterator{nullptr};
}

template <typename T>
T& List<T>::operator[](int index)
{
    static T element;

    if(head_ == nullptr)
        exit(E_MODIFYING_ITEM);

    auto tmp = head_;
    for(int i = 0; i < index; ++i)
        tmp = tmp->next_;

    element = tmp->data_;

    return element;
}
template <typename T>
int List<T>::size()
{
    int size = 0;

    if(head_ == nullptr)
    {
        return size;
    }
    else
    {
        auto tmp = head_;
        for(size = 0; tmp->next_ != nullptr; ++size)
        {
            tmp = tmp->next_;
        }

        return size;
    }
}
template <typename T>
bool List<T>::isEmpty()
{
    if(head_ == nullptr)
        return true;

    return false;
}

#endif /* LIST_HPP_ */
