#ifndef QUEUE_HPP_
#define QUEUE_HPP_

template <typename T>
class Queue
{
    List<T> queue;

  public:
    void enqueue(const T& newElement);
    T dequeue();
};

template <typename T>
void Queue<T>::enqueue(const T& newElement)
{
    queue.pushBack(newElement);
}

template <typename T>
T Queue<T>::dequeue()
{
    T element = queue[0];
    queue.removeOnce(element);

    return element;
}

#endif /* QUEUE_HPP_ */
