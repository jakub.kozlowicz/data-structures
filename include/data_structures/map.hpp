#ifndef MAP_HPP_
#define MAP_HPP_

constexpr int SIZE_MAP = 1000;

template <typename KeyType, typename ValueType>
class Map
{
    std::array<ValueType, SIZE_MAP> map_;
    std::array<bool, SIZE_MAP> isElement_;

    int hash(const std::string& string);
    int hash(const char& sign);

  public:
    Map();
    Map(const Map& otherMap);

    void insert(const KeyType& key, const ValueType& value);
    ValueType& operator[](const KeyType& key);
    void remove(const KeyType& key);
};

template <typename KeyType, typename ValueType>
int Map<KeyType, ValueType>::hash(const std::string& string)
{
    int result = 0;
    for(char i : string)
    {
        result += static_cast<int>(i);
    }

    return result % SIZE_MAP;
}

template <typename KeyType, typename ValueType>
int Map<KeyType, ValueType>::hash(const char& sign)
{
    int result = 0;
    result += static_cast<int>(sign);

    return result % SIZE_MAP;
}

template <typename KeyType, typename ValueType>
Map<KeyType, ValueType>::Map()
{
    for(int i = 0; i < SIZE_MAP; ++i)
    {
        map_[i] = 0;
        isElement_[i] = false;
    }
}

template <typename KeyType, typename ValueType>
Map<KeyType, ValueType>::Map(const Map& otherMap)
{
    for(int i = 0; i < SIZE_MAP; ++i)
    {
        map_[i] = otherMap.map_[i];
        isElement_[i] = otherMap.isElement_[i];
    }
}

template <typename KeyType, typename ValueType>
void Map<KeyType, ValueType>::insert(const KeyType& key, const ValueType& value)
{
    int index = hash(key);
    map_[index] = value;
    isElement_[index] = true;
}

template <typename KeyType, typename ValueType>
ValueType& Map<KeyType, ValueType>::operator[](const KeyType& key)
{
    return map_[hash(key)];
}

template <typename KeyType, typename ValueType>
void Map<KeyType, ValueType>::remove(const KeyType& key)
{
    int index = hash(key);
    map_[index] = 0;
    isElement_[index] = false;
}

#endif /* MAP_HPP_ */
