#ifndef STACK_HPP_
#define STACK_HPP_

template <typename T>
class Stack
{
    List<T> stack;

  public:
    void push(const T& newElement);
    T pop();
};

template <typename T>
void Stack<T>::push(const T& newElement)
{
    stack.pushFront(newElement);
}

template <typename T>
T Stack<T>::pop()
{
    T element = stack[0];
    stack.removeOnce(element);

    return element;
}

#endif /* STACK_HPP_ */
