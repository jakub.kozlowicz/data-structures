#ifndef PRIORITY_QUEUE_HPP_
#define PRIORITY_QUEUE_HPP_

template <typename T>
class PriorityQueue
{
  private:
    int findIndex(const int& priority);

  public:
    struct PriorityData
    {
        int priority_;
        T priorityData_;

        bool operator!=(const PriorityData& otherPriorityData) const
        {
            return (this->priorityData_ != otherPriorityData.priorityData_ &&
                    this->priority_ != otherPriorityData.priority_);
        }
        bool operator>(const int& number) const { return (this->priority_ > number); }
    };

    void enqueue(const T& newElement, int priority);
    T dequeue();

  private:
    List<PriorityData> priorityQueue_;
};

template <typename T>
int PriorityQueue<T>::findIndex(const int& priority)
{
    int index = 0;

    if(priorityQueue_.isEmpty() || (priorityQueue_.size() == 0 && !(priorityQueue_[index] > priority)))
    {
        return index;
    }
    else if(priorityQueue_.size() == 0 && priorityQueue_[index] > priority)
    {
        return index + 1;
    }
    else
    {
        for(index = 0; index < priorityQueue_.size(); ++index)
        {
            if(!(priorityQueue_[index] > priority))
            {
                return index;
            }
        }
        return index + 1;
    }
}

template <typename T>
void PriorityQueue<T>::enqueue(const T& newElement, int priority)
{
    priorityQueue_.insert(PriorityData{priority, newElement}, findIndex(priority));
}

template <typename T>
T PriorityQueue<T>::dequeue()
{
    PriorityData element = priorityQueue_[0];
    priorityQueue_.removeOnce(element);

    return element.priorityData_;
}

#endif /* PRIORITY_QUEUE_HPP_ */
