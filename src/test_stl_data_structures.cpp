#include "catch2/catch.hpp"

#include <list>
#include <stack>
#include <queue>
#include <map>

using namespace std::string_literals;

TEST_CASE("std::list/push_front")
{
    std::list<int> list;

    list.push_front(1);
    list.push_front(2);

    REQUIRE(list.front() == 2);
    REQUIRE(list.back() == 1);
}

TEST_CASE("std::list/push_back")
{
    std::list<int> list;

    list.push_back(1);
    list.push_back(2);

    REQUIRE(list.front() == 1);
    REQUIRE(list.back() == 2);
}

TEST_CASE("std::list/insert")
{
    std::list<int> list;

    list.push_back(1);
    list.push_back(2);

    list.insert(++list.begin(), 3);
    list.insert(list.begin(), 4);

    auto it = list.begin();

    REQUIRE(*(it) == 4);
    REQUIRE(*(++it) == 1);
    REQUIRE(*(++it) == 3);
    REQUIRE(*(++it) == 2);
}

TEST_CASE("std::list/remove")
{
    std::list<int> list;

    list.push_back(1);
    list.push_back(2);
    list.push_back(3);

    list.remove(2);

    auto it = list.begin();

    REQUIRE(*it == 1);
    REQUIRE(*(++it) == 3);

    list.remove(1);
    REQUIRE(list.front() == 3);
}

TEST_CASE("std::list/iterator")
{
    std::list<int> list;

    list.push_back(1);
    list.push_back(3);
    list.push_back(2);

    list.sort();

    auto it = list.begin();

    REQUIRE(*it == 1);
    REQUIRE(*(++it) == 2);
    REQUIRE(*(++it) == 3);
}

TEST_CASE("std::list/const_iterator")
{
    std::list<int> list;

    std::array<int, 3> refArray = {1, 2, 3};

    for(auto& element : refArray)
    {
        list.push_back(element);
    }

    int index = 0;
    for(auto it = list.cbegin(); it != list.cend(); ++it)
    {
        REQUIRE(*it == refArray[index++]);
    }
}

TEST_CASE("std::stack")
{
    std::stack<int> stack;

    stack.push(1);
    stack.push(2);
    stack.push(3);

    REQUIRE(stack.top() == 3);
    stack.pop();

    REQUIRE(stack.top() == 2);
    stack.pop();

    REQUIRE(stack.top() == 1);
}

TEST_CASE("std::queue")
{
    std::queue<int> queue;

    queue.push(1);
    queue.push(2);
    queue.push(3);

    REQUIRE(queue.front() == 1);
    queue.pop();

    REQUIRE(queue.front() == 2);
    queue.pop();

    REQUIRE(queue.front() == 3);
}

TEST_CASE("std::priority_queue")
{
    std::priority_queue<int> queue;

    queue.push(1);
    queue.push(3);
    queue.push(2);

    REQUIRE(queue.top() == 3);
    queue.pop();

    REQUIRE(queue.top() == 2);
    queue.pop();

    REQUIRE(queue.top() == 1);
}

TEST_CASE("std::map")
{
    std::map<std::string, int> hashArray;

    std::map<std::string, int> refHashArray = {{"one"s, 1}, {"two"s, 2}, {"three"s, 3}};

    for(auto& keyValue : refHashArray)
    {
        hashArray.insert(keyValue);
    }

    for(auto& keyValue : refHashArray)
    {
        REQUIRE(keyValue.second == hashArray[keyValue.first]);
    }
}